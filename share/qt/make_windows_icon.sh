#!/bin/bash
# create multiresolution windows icon
ICON_DST=../../src/qt/res/icons/CredenceCoin.ico

convert ../../src/qt/res/icons/CredenceCoin-16.png ../../src/qt/res/icons/CredenceCoin-32.png ../../src/qt/res/icons/CredenceCoin-48.png ${ICON_DST}
